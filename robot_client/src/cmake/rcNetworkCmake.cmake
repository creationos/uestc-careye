message(STATUS "Load RC Network")
set(LIB_NAME rcnetwork)
set(RCNETWORK_SOURCES_DIR ${PROJECT_SOURCE_DIR}/src/sources/rc_network)
set(RCNETWORK_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/src/include/rc_network)

file(GLOB RCNETWORK_INCLUDE ${RCNETWORK_INCLUDE_DIR}/*.h)
file(GLOB RCNETWORK_SOURCES ${RCNETWORK_SOURCES_DIR}/*.cpp)
include_directories(${PISTACHE_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${PROJECT_SOURCE_DIR}/../3rdparty/grpc/include)
include_directories(${PROJECT_SOURCE_DIR}/../3rdparty/grpc/third_party/protobuf)
message(STATUS "Find PISTACHE${PISTACHE_INCLUDE_DIRS}")

foreach (FILE ${RCNETWORK_INCLUDE})
    message(STATUS "RC_NETWORK${FILE}")
endforeach ()

foreach (FILE ${RCNETWORK_SOURCES})
    message(STATUS ${FILE})
endforeach ()
set(RC_NETWORK_FILES
        ${RCNETWORK_SOURCES}
        ${RCNETWORK_INCLUDE}
        )

