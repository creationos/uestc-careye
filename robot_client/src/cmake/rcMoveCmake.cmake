message(STATUS "Load Move Module")
set(RCMOVE_DIR ${PROJECT_SOURCE_DIR}/src)
file(GLOB RC_MOVE_FILES ${RCMOVE_DIR}/sources/rc_move/*.cpp)
foreach(FILE ${RC_MOVE_FILES})
    message(STATUS "RC_MOVE: ${FILE}")
endforeach()
include_directories(${RCMOVE_DIR}/include)


