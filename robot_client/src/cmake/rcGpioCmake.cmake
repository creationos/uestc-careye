message(STATUS "Load GPIO Module")
set(RCGPIO_DIR ${PROJECT_SOURCE_DIR}/src)

file(GLOB RC_GPIO_FILES ${RCGPIO_DIR}/sources/rc_gpio/*.cpp)

set(RC_GPIO_FILES
        ${RC_GPIO_FILES}
        )
foreach(FILE ${RC_GPIO_FILES})
    message(STATUS "RC_GPIO:${FILE}")
endforeach()
include_directories(${RCGPIO_DIR}/include)

