set(TEST_SOURCES_DIR ${PROJECT_SOURCE_DIR}/src/sources/rc_test)
if (RC_TEST)
    find_package(PkgConfig REQUIRED)

    pkg_check_modules(GTK3 REQUIRED gtk+-2.0)
    include_directories(${GTK2_INCLUDE_DIRS})
    link_directories(${GTK2_LIBRARY_DIRS})

    pkg_check_modules(GTKMM2 gtkmm-2.4)
    link_directories(${GTKMM2_LIBRARY_DIRS})
    include_directories(${GTKMM3_INCLUDE_DIRS})

    pkg_check_modules(GLEW glew2.0)
    link_directories(${GLEW_LIBRARY_DIRS})
    include_directories(${GLEW_INCLUDE_DIRS})

    file(GLOB TEST_SOURCES_FILES ${TEST_SOURCES_DIR}/*.cpp)
    foreach (TEST_EXE ${TEST_SOURCES_FILES})
        get_filename_component(mainname ${TEST_EXE} NAME_WE)
        message(STATUS "TEST_EXE:${mainname}")

        add_executable(${mainname} ${TEST_EXE})
        target_link_libraries(${mainname}
                ${OpenCV_LIBS}
                ${Boost_LIBRARIES}
                ${MPI_CXX_LIBRARIES}
                ${OPENGL_LIBRARIES}
                ${GLUT_LIBRARY}
                ${GTK2_LIBRARIES}
                ${GTKMM2_LIBRARIES}
                ${Pistache_LIBRARIES}/libpistache.a
                ${realsense2_LIBRARY}
#                GLEW
                GLU
                libserv
                pthread
#                gtkgl-2.0
                libprotobuf
                grpc
                grpc++
                grpc++_alts
                grpc++_unsecure
                grpc++_error_details
                grpc++_reflection
                rc
                )
    endforeach ()
endif ()

