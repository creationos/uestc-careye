//
// Created by PulsarV on 18-10-30.
//

#include <rc_message/rc_radar_msg.h>

namespace RC {
    namespace Message {
        boost::mutex RadarMessage::radar_mutex;
        RadarMessage::RadarMessage(int max_queue_size)
                : BaseMessage<std::vector<RC::Map::DOT>>(max_queue_size) {
        }
    }
}