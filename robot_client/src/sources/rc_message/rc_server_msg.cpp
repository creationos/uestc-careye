//
// Created by PulsarV on 18-10-30.
//
#include <rc_message/rc_msg_server.h>

namespace RC{
    namespace Message{
        ImageMessage MessageServer::imageMessage(3);
        SerialMessage MessageServer::serialMessage(3);
        NetworkMessage MessageServer::networkMessage(3);
        MoveMessage MessageServer::moveMessage(3);
        RadarMessage MessageServer::radarMessage(2);
        GyroMessage MessageServer::gyroMessage(2);
    }
}
