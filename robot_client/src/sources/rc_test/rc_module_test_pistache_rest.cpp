#include <rc_network/rc_httpd_rest.h>
using namespace RC::Network::Httpd;
using namespace std;
int main(int argc, char *argv[]) {
    int thr = 4;


    cout << "CPU核心数 = " << hardware_concurrency() << endl;
    cout << "Using " << thr << " threads" << endl;

    rcHttpdRest rcHttpdRest(9800);

    rcHttpdRest.init(thr);
    rcHttpdRest.start();
}
