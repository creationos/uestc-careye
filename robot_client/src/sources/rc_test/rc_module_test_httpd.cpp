//
// Created by Pulsar on 2020/5/16.
//
#include <rc_network/rc_httpd.h>
int main(int argc, char **argv) {
    RC::Network::Httpd::HttpdServer httpdServer;
    httpdServer.start_server(9800);
    return 1;
}