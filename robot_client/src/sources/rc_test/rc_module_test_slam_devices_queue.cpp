//
// Created by Pulsar on 2020/5/16.
//
#include <rc_task/rcRadarTask.h>
#include <iostream>
#include <signal.h>

using namespace RC::Task;

bool ctrl_c_pressed;

void ctrlc(int) {
    ctrl_c_pressed = true;
}

int main(int argc, char **argv) {
    signal(SIGINT, ctrlc);
    rc_RadarInfo rcRadarInfo;
    rcRadarInfo.radar_device=(char*)"/dev/ttyUSB0";
    run_radar_task(rcRadarInfo);
    return 1;
}