//
// Created by Pulsar on 2020/5/16.
//
#include <rc_move/rcmove.h>
#include <rc_task/rcTaskManager_DataStruct.h>
#include <iostream>
#include <base/slog.hpp>

/**
 * 本测试步骤分为
 * 1. 初始化测试
 * 2. 卸载测试
 * 3. 转动测试
 */
int main(int argc, char **argv) {
    //初始化测试
    RC::Task::rc_MoveDevice rcMoveDevice;
    rcMoveDevice.gpio_1 = 402;
    rcMoveDevice.gpio_2 = 405;
    rcMoveDevice.gpio_3 = 431;
    rcMoveDevice.pwm_1 = 1;
    rcMoveDevice.pwm_2 = 3;
    rcMoveDevice.pwm_3 = 0;
    slog::info << ">>>启动制动模块" << slog::endl;
    slog::info << "gpio1: " << rcMoveDevice.gpio_1 << " pwm1: " << rcMoveDevice.pwm_1 << slog::endl;
    slog::info << "gpio2: " << rcMoveDevice.gpio_2 << " pwm2: " << rcMoveDevice.pwm_2 << slog::endl;
    slog::info << "gpio3: " << rcMoveDevice.gpio_3 << " pwm3: " << rcMoveDevice.pwm_3 << slog::endl;

    RC::RobotCarMove robotCarMove;
    robotCarMove.init(rcMoveDevice);

    char exit_c = 'a';
    while (exit_c != 'c') {
        exit_c = getchar();
        robotCarMove.command(exit_c,3413333,3413333,3413333);
    }
    slog::info << "退出" << slog::endl;
    return 1;
}