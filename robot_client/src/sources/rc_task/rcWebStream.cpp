//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcWebStream.h>
#include <iostream>
#include <base/slog.hpp>
#include <rc_network/rc_httpd_rest.h>

namespace RC {
    namespace Task {
        void run_httpd_task(rc_ServerInfo rcServerInfo) {
            using namespace RC::Network::Httpd;

            slog::info << "WEB任务启动中" << slog::endl;
            int thr = 4;

            slog::info << "CPU核心数 = " << hardware_concurrency() << slog::endl;
            slog::info << "HTTP服务线程 " << thr << " 个" << slog::endl;
            try {
                rcHttpdRest rcHttpdRest(rcServerInfo.httpd_port);
                rcHttpdRest.init(thr);
                rcHttpdRest.start();
            } catch (std::runtime_error &e) {
                slog::err << "HTTP启动失败 " << slog::endl;
                slog::err << e.what() << slog::endl;
            }
        }
    }
}

