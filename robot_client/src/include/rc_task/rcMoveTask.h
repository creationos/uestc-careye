//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCMOVETASK_H
#define UESTC_CAREYE_RCMOVETASK_H

#include <rc_task/rcTaskManager_DataStruct.h>

namespace RC {
    namespace Task {
        /***
         * 制动任务
         * @param device_info
         */
        void run_move_task(rc_MoveDevice rcMoveDevice);
    }
}


#endif //UESTC_CAREYE_RCMOVETASK_H
