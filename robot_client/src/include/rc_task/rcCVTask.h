// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCCVTASK_H
#define UESTC_CAREYE_RCCVTASK_H

#include <rc_task/rcTaskManager_DataStruct.h>
namespace RC {
    namespace Task {
        /**
         * 视觉设备任务
         * @param rcRadarInfo
         */
        void run_cv_task(rc_DeviceInfo rcDeviceInfo);
    }
}


#endif //UESTC_CAREYE_RCCVTASK_H
