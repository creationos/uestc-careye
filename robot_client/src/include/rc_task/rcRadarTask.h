//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCRADARTASK_H
#define UESTC_CAREYE_RCRADARTASK_H
#include <rc_task/rcTaskManager_DataStruct.h>

namespace RC {
    namespace Task {
        /**
         * 雷达任务
         * @param rcRadarInfo
         */
        void run_radar_task(rc_RadarInfo rcRadarInfo);
    }
}


#endif //UESTC_CAREYE_RCRADARTASK_H
