//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCFRAMEQUEUE_H
#define ROBOCAR_RCFRAMEQUEUE_H
#include <opencv2/opencv.hpp>
#include <queue>
#include <rc_task/rcTaskManager_DataStruct.h>
namespace RC {
    namespace Task {
        /**
         * web服务任务
         * @param rcServerInfo
         */
        void run_httpd_task(rc_ServerInfo rcServerInfo);
        /**
         * websocket服务任务
         * @param rcServerInfo
         */
        void run_websocket_task(rc_ServerInfo rcServerInfo);
    }
}
#endif //ROBOCAR_RCFRAMEQUEUE_H
