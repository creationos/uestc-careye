//
// Created by PulsarV on 18-5-14.
//


#ifndef ROBOCAR_RC_PARAM_H
#define ROBOCAR_RC_PARAM_H
#include <gflags/gflags.h>



/// @brief message for help argument
static const char help_message[] = "Print a usage message.";

/// @brief message for generate default config file
static const char generate_message[] = "Generate default config file.";

/// @brief message for config argument
static const char config_message[] = "Path to .xml config file. Default is config.xml";


//帮助信息
DEFINE_bool(h, false, help_message);

//初始化配置
DEFINE_bool(generate, false, help_message);

//加载配置文件
DEFINE_string(c, "", config_message);

static void showUsage() {
    std::cout << std::endl;
    std::cout << "Careye [OPTION]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << std::endl;
    std::cout << "    -h                        " << help_message << std::endl;
    std::cout << "    -generate                 " << generate_message << std::endl;
    std::cout << "    -c \"<path>\"               " << config_message << std::endl;
}
static void generateDefaultConfig(){
    cv::FileStorage fswrite("config.xml",cv::FileStorage::WRITE);
    fswrite.writeComment("Default Config File", true);
    fswrite.write("device","CPU");

//    <!--摄像头信息-->
//    <camera>
//        <type>1</type>
//        <index>1</index>
//        <ipaddress>0.0.0.0</ipaddress>
//        <port>1080</port>
//    </camera>
    fswrite<<"camera"<<"{";
    fswrite.write("index",0);
    fswrite.write("type",RC_CAMERA_DEVICE);
    fswrite.write("filepath","");
    fswrite.write("message_queue","Camera");
    fswrite<<"}";

//    <!--计算服务器地址-->
//    <server>
//        <ipaddress>0.0.0.0</ipaddress>
//        <port>1080</port>
//    </server>
    fswrite<<"server"<<"{";
    fswrite.write("port",1080);
    fswrite.write("ipaddress","0.0.0.0");
    fswrite<<"}";

//    <!--WEB服务器-->
//    <httpd>
//        <ipaddress>0.0.0.0</ipaddress>
//        <port>1080</port>
//    </httpd>
    fswrite<<"httpd"<<"{";
    fswrite.write("port",9800);
    fswrite<<"}";

//    <!--WEBSOCKER配置-->
//    <websocket>
//        <ipaddress>0.0.0.0</ipaddress>
//        <port>1080</port>
//    </websocket>
    fswrite<<"websocket"<<"{";
    fswrite.write("port",1090);
    fswrite<<"}";

//    <!--串口-->
//    <serial>
//        <path>/dev/ttyUSB0</path>
//        <freq>9600</freq>
//    </serial>
    fswrite<<"serial"<<"{";
    fswrite.write("path","/dev/ttyUSB0");
    fswrite.write("freq",9600);
    fswrite<<"}";

//    <!--雷达-->
//    <radar>
//        <path>/dev/ttyUSB1</path>
//        <!--转速-->
//        <revolution>200</revolution>
//    </radar>
    fswrite<<"radar"<<"{";
    fswrite.write("path","/dev/ttyUSB0");
    fswrite<<"}";

//    <!--车轮转速比率(PID控制使用)-->
//    <wheel>
//        <main_wheel>wheel_1</main_wheel>
//        <wheel_1>1</wheel_1>
//        <wheel_2>0.8</wheel_2>
//        <wheel_3>0.8</wheel_3>
//    </wheel>
    fswrite<<"wheel"<<"{";
    fswrite.write("main_wheel",1);
    fswrite.write("wheel_1",1);
    fswrite.write("wheel_2",0.8);
    fswrite.write("wheel_3",0.8);
    fswrite<<"}";

//    <!--陀螺仪-->
//    <serial>
//        <path>/dev/ttyUSB0</path>
//        <freq>9600</freq>
//    </serial>
    fswrite<<"gyroscope"<<"{";
    fswrite.write("path","/dev/ttyS5");
    fswrite.write("freq",9600);
    fswrite.write("head",0x55);
    fswrite<<"}";

//    <!--地图文件-->
//    <map>
//        <path>mapping.bin</path>
//    </map>
    fswrite<<"map"<<"{";
    fswrite.write("path","mapping.bin");
    fswrite<<"}";

//    <!--GPIO配置-->
//    <gpio>
//        <pwm_0>0</pwm_0>
//        <dist_0>402</dist_0>
//        <pwm_1>1</pwm_1>
//        <dist_2>405</dist_2>
//        <pwm_2>3</pwm_2>
//        <dist_2>431</dist_2>
//    </gpio>
    //车轮1 PWM和方向控制IO
    fswrite<<"gpio"<<"{";
    fswrite.write("pwm_0",0);
    fswrite.write("dist_0",402);
    //车轮2 PWM和方向控制IO
    fswrite.write("pwm_1",1);
    fswrite.write("dist_1",405);
    //车轮3 PWM和方向控制IO
    fswrite.write("pwm_2",3);
    fswrite.write("dist_2",431);
    fswrite<<"}";

    //模型陆军
    fswrite<<"model"<<"{";
    fswrite.write("license-plate-recognition-barrier","models/ir/intel/license-plate-recognition-barrier-0001/FP16/license-plate-recognition-barrier-0001.xml");
    fswrite.write("vehicle-attributes-recognition-barrier","models/ir/intel/vehicle-attributes-recognition-barrier-0039/FP16/vehicle-attributes-recognition-barrier-0039.xml");
    fswrite.write("vehicle-license-plate-detection-barrier","models/ir/intel/vehicle-license-plate-detection-barrier-0106/FP16/vehicle-license-plate-detection-barrier-0106.xml");
    fswrite.write("road-segmentation-adas","models/ir/intel/road-segmentation-adas-0001/FP16/road-segmentation-adas-0001.xml");
    fswrite.write("single-image-super-resolution","models/ir/intel/single-image-super-resolution-1032/FP16/single-image-super-resolution-1032.xml");
    fswrite<<"}";
    fswrite.release();
}

#endif