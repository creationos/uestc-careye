//
// Created by PulsarV on 18-7-5.
//

#ifndef ROBOCAR_RCMAPPING_H
#define ROBOCAR_RCMAPPING_H

#include <rc_globalVarable/rc_global.h>
#include <rc_mapping/slam_devices.h>
#include <string>

namespace RC {
    namespace Map {
        class RcMapping : public SlamDevice {
        public:
            RcMapping(const std::string &mapfile, const std::string &device_path);

        private:
            char *map_path;
        };
    }
}


#endif //ROBOCAR_RCMAPPING_H
