//
// Created by PulsarV on 18-5-9.
//

#ifndef ROBOCAR_RCMOVE_DATA_STRUCT_H
#define ROBOCAR_RCMOVE_DATA_STRUCT_H
//车轮控制数据
typedef struct {
    int wheel_1_speed=0;
    int wheel_2_speed=0;
    int wheel_3_speed=0;
    int weel_1_direction=0;
    int weel_2_direction=0;
    int weel_3_direction=0;
}WHEEL_DATA;
enum {
    RC_MOVE_BACKWARD=0,
    RC_MOVE_FORWARD
};
#endif //ROBOCAR_RCMOVE_DATA_STRUCT_H
