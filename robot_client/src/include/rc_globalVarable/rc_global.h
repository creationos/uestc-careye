//
// Created by PulsarV on 18-5-10.
//

#define RADIAN CV_PI/180.0
#define LOADING_IMAGE "background.jpg"

#ifndef ROBOCAR_RC_GLOBAL_H
#define ROBOCAR_RC_GLOBAL_H

#define TEST 0
enum {
    RC_CAMERA_DEVICE=0,//USB摄像头
    RC_VIDEO_FILE, //文件
    RC_WEBCAM //网络摄像头
};
enum {
    RC_PLAY_BY_CAMERA=0,
    RC_PLAY_BY_VIDEO
};

//#define RC_BODY_CASCADES_FILE_PATH "./haarcascades/haarcascade_fullbody.xml"
#define RC_BODY_CASCADES_FILE_PATH "/opt/openvino/openvino_2020.2.120/opencv/etc/haarcascades/haarcascade_frontalcatface.xml"
#define RC_FACE_CASCADES_FILE_PATH "./haarcascades/haarcascade_frontalcatface.xml"

#define RUN_STATE std::cout<<"\033[1;32;31m ON: "<<__FILE__\
<<" "<<__FUNCTION__<<"()"<<" "<<__LINE__<<"\033[0m"<<std::endl;

#ifdef __linux__
#define RC_NULL_POINT nullptr
#endif
#endif //ROBOCAR_RC_GLOBAL_H
