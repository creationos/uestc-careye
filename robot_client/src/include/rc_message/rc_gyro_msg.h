//
// Created by Pulsar on 2020/5/30.
//

#ifndef UESTC_CAREYE_RC_GYRO_MSG_H
#define UESTC_CAREYE_RC_GYRO_MSG_H

#include <boost/thread/mutex.hpp>
//这是一个比较特殊的类，因为陀螺仪的数据不一定一致抵达
namespace RC {
    namespace Message {
        class GyroMessage{
        public:
            explicit GyroMessage(int max_queue_size);

            static boost::mutex gyro_mutex;
        };
    }
}


#endif //UESTC_CAREYE_RC_GYRO_MSG_H
