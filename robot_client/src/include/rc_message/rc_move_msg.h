//
// Created by Pulsar on 2020/5/6.
//

#ifndef ROBOCAR_RC_MOVE_MSG_H
#define ROBOCAR_RC_MOVE_MSG_H

#include <rc_message/rc_base_msg.hpp>
#include <map>
#include <rc_move/rcmove_data_struct.h>
#include <boost/thread/mutex.hpp>
namespace RC {
    namespace Message {
        class MoveMessage : public BaseMessage<WHEEL_DATA> {
        public:
            explicit MoveMessage(int max_queue_size);
            static boost::mutex move_mutex;
        };
    }
}


#endif //ROBOCAR_RC_MOVE_MSG_H
