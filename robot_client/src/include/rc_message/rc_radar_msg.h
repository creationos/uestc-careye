//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_RADAR_MSG_H
#define ROBOCAR_RM_RADAR_MSG_H

#include <rc_task/rcTaskManager_DataStruct.h>
#include <rc_message/rc_base_msg.hpp>
#include <map>
#include <rc_mapping/rcmapping.h>
#include <boost/thread/mutex.hpp>

namespace RC {
    namespace Message {
        using namespace RC::Map;

        class RadarMessage : public BaseMessage<std::vector<RC::Map::DOT>> {
        public:
            explicit RadarMessage(int max_queue_size);

            static boost::mutex radar_mutex;
        };
    }
}

#endif //ROBOCAR_RM_RADAR_MSG_H
