//
// Created by Pulsar on 2020/5/15.
//

#include <rc_gpio/c_gpio.h>
#include <rc_gpio/cpuinfo.h>
#include <rc_gpio/soft_pwm.h>
#include <rc_gpio/event_gpio.h>
#include <iostream>
#include <base/slog.hpp>
void init_pwm(int pwm_1,int pwm_2,int pwm_3) {
    RC::GPIO::pwm_begin(pwm_1);
    RC::GPIO::pwm_set_frequency(pwm_1, 3413333);
    RC::GPIO::pwm_set_duty_cycle(pwm_1, 1706667);
    RC::GPIO::pwm_start(pwm_1);

    RC::GPIO::pwm_begin(pwm_2);
    RC::GPIO::pwm_set_frequency(pwm_2, 3413333);
    RC::GPIO::pwm_set_duty_cycle(pwm_2, 1706667);
    RC::GPIO::pwm_start(pwm_2);

    RC::GPIO::pwm_begin(pwm_3);
    RC::GPIO::pwm_set_frequency(pwm_3, 3413333);
    RC::GPIO::pwm_set_duty_cycle(pwm_3, 1706667);
    RC::GPIO::pwm_start(pwm_3);

    RC::GPIO::pwm_stop(pwm_1);
    RC::GPIO::pwm_stop(pwm_2);
    RC::GPIO::pwm_stop(pwm_3);
}

void init_direction(int gpio_1,int gpio_2,int gpio_3) {
    RC::GPIO::pinMode(gpio_1, RC_GPIO_OUTPUT);
    RC::GPIO::digitalWrite(gpio_1, RC_GPIO_LOW);

    RC::GPIO::pinMode(gpio_2, RC_GPIO_OUTPUT);
    RC::GPIO::digitalWrite(gpio_2, RC_GPIO_LOW);

    RC::GPIO::pinMode(gpio_3, RC_GPIO_OUTPUT);
    RC::GPIO::digitalWrite(gpio_3, RC_GPIO_LOW);
}
int main(int argc, char **argv) {
    //初始化测试
    int gpio_1=402,gpio_2=405,gpio_3=431;
    int pwm_1=1,pwm_2=3,pwm_3=0;
    init_direction(gpio_1,gpio_2,gpio_3);
    init_pwm(pwm_1,pwm_2,pwm_3);
    slog::info << "初始化引脚" << slog::endl;
    slog::info << "gpio1: " << gpio_1 << " pwm1: " << pwm_1 << slog::endl;
    slog::info << "gpio2: " << gpio_2 << " pwm2: " << pwm_2 << slog::endl;
    slog::info << "gpio3: " << gpio_3 << " pwm3: " << pwm_3 << slog::endl;
    slog::info << "退出" << slog::endl;
    return 1;
}