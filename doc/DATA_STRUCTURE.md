# 串口数据
<table>
<tr>
    <td>数据包类型</td>
    <td>十六进制值</td>
    <td>十进制</td>
</tr>
<tr>
    <td>握手请求</td>
    <td>0x10</td>
    <td>16</td>
</tr>
<tr>
    <td>图像</td>
    <td>0x11</td>
    <td>17</td>
</tr>
<tr>
    <td>加速度</td>
    <td>0x12</td>
    <td>18</td>
</tr>
<tr>
    <td>角加速度</td>
    <td>0x13</td>
    <td>19</td>
</tr>
<tr>
    <td>磁场</td>
    <td>0x14</td>
    <td>20</td>
</tr>
<tr>
    <td>文件</td>
    <td>0x15</td>
    <td>21</td>
</tr>
<tr>
    <td>电量</td>
    <td>0x16</td>
    <td>22</td>
</tr>
<tr>
    <td>结束符</td>
    <td>0x17</td>
    <td>23</td>
</tr>
</table>

## 握手包 
 
```c++
typedef struct _touch{
    uchar head;
    uchar type_h;
    uchar type_l;
    uchar len_hh;
    uchar len_hl;
    uchar len_lh;
    uchar len_ll;
    uchar sum;
}touch;
```
<table>
<tr>
    <td>数据包头</td>
    <td>数据包类型H</td>
    <td>数据包类型L</td>
    <td>包长度</td>
    <td>包长度</td>
    <td>包长度</td>
    <td>包长度</td>
    <td>校验和</td>
</tr>
<tr>
    <td>0x10</td>
    <td>TYPE_H</td>
    <td>TYPE_L</td>
    <td>HH</td>
    <td>HL</td>
    <td>LH</td>
    <td>LL</td>
    <td>SUM</td>
</tr>
</table>

## 数据体

#TCP数据

TCP数据分为报头和请求体  

<table>
<tr>
    <td>数据包头</td>
    <td>数据包类型H</td>
    <td>数据包类型L</td>
    <td>包长度</td>
    <td>包长度</td>
    <td>包长度</td>
    <td>包长度</td>
    <td>校验和</td>
</tr>
<tr>
    <td>0x10</td>
    <td>TYPE_H</td>
    <td>TYPE_L</td>
    <td>HH</td>
    <td>HL</td>
    <td>LH</td>
    <td>LL</td>
    <td>SUM</td>
</tr>
</table>
