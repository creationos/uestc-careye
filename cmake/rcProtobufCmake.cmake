include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../3rdparty/grpc/third_party/protobuf/src/)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
function(PROTOBUF_GENERATE SRCS HDRS)
    if(NOT ARGN)
        message(SEND_ERROR "Error: PROTOBUF_GENERATE_GRPC_CPP() called without any proto files")
        return()
    endif()

    set(${SRCS})
    set(${HDRS})
    foreach(FIL ${ARGN})
        get_filename_component(ABS_FIL ${FIL} ABSOLUTE) # 绝对路径
        get_filename_component(FIL_WE ${FIL} NAME_WE) # 文件名

        list(APPEND ${SRCS} "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.pb.cc")
        list(APPEND ${HDRS} "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.pb.h")
        list(APPEND ${SRCS} "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.grpc.pb.cc")
        list(APPEND ${HDRS} "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.grpc.pb.h")

        message("Command:${CMAKE_CURRENT_BINARY_DIR}/../bin/protoc -I ${CMAKE_SOURCE_DIR}/resource --plugin=protoc-gen-grpc=${CMAKE_CURRENT_BINARY_DIR}/../bin/grpc_cpp_plugin --grpc_out ${PROJECT_BINARY_DIR} ${ABS_FIL}" )
        add_custom_command(
                OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.grpc.pb.cc"
                "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.grpc.pb.h"
                PRE_BUILD
                COMMAND   protoc -I ${CMAKE_SOURCE_DIR}/resource --plugin=protoc-gen-grpc=${CMAKE_CURRENT_BINARY_DIR}/../bin/grpc_cpp_plugin --grpc_out ${PROJECT_BINARY_DIR} ${ABS_FIL}
                COMMAND   protoc -I ${CMAKE_SOURCE_DIR}/resource  --cpp_out ${PROJECT_BINARY_DIR} ${ABS_FIL}
                DEPENDS ${ABS_FIL} protoc grpc_cpp_plugin
                COMMENT "protoc -I ${CMAKE_SOURCE_DIR}/resource --plugin=protoc-gen-grpc=${CMAKE_CURRENT_BINARY_DIR}/../bin/grpc_cpp_plugin --grpc_out ${PROJECT_BINARY_DIR} ${ABS_FIL}"
                )
    endforeach()

    set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
    set(${SRCS} ${${SRCS}} PARENT_SCOPE)
    set(${HDRS} ${${HDRS}} PARENT_SCOPE)
endfunction()

message(STATUS "Generate Proto File")

set(RCPROTO_DIR ${PROJECT_SOURCE_DIR}/../resource)
file(GLOB RC_PROTO_FILES ${RCPROTO_DIR}/*.proto)

PROTOBUF_GENERATE(PROTO_SRC PROTO_H ${RC_PROTO_FILES})

foreach(FILE ${RC_PROTO_FILES})
    message(STATUS "RC_PROTO_FILE: ${FILE}")
endforeach()
foreach(FILE ${PROTO_SRC})
    message(STATUS "PROTO_SRC: ${FILE}")
endforeach()
foreach(FILE ${PROTO_H})
    message(STATUS "PROTO_H: ${FILE}")
endforeach()

