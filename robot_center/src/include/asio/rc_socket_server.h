//
// Created by pulsarv on 19-5-2.
//
#ifndef UESTC_CAREYE_RC_SOCKET_SERVER_H
#define UESTC_CAREYE_RC_SOCKET_SERVER_H

#include <QTcpServer>
#include <QTcpSocket>

class   rc_socket_server : public QTcpServer {

Q_OBJECT

public:
    explicit rc_socket_server(QObject *parent = 0);

    explicit rc_socket_server(int port, int max_buffer_size,QObject *parent = 0);

    void set_server_properties(int port, int max_buffer_size);

    void bind_call(void(*bind_function)(void *));

    void start_listen();

    ~rc_socket_server();

public:
    int port;
    int max_buffer_size;
    QTcpServer *tcp_server;
    QTcpSocket *tcp_socket;

    void (*bind_function)(void *);

private slots:

    void server_new_connection();

    void server_read_data();

    void server_disconnect();

};


#endif //UESTC_CAREYE_RC_SOCKET_SERVER_H
