#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <boost/asio.hpp>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {

Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private slots:
    void on_pushButton_start_server_clicked();
    void on_pushButton_Q_clicked();
    void on_pushButton_W_clicked();
    void on_pushButton_A_clicked();
    void on_pushButton_S_clicked();
    void on_pushButton_D_clicked();
    void on_pushButton_R_clicked();
    void on_pushButton_E_clicked();
    void on_clicked();

private:
    Ui::MainWindow *ui;
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
